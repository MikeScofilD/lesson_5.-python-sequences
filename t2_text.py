# Задание 3
# Напишите программу,
# которая вводит с клавиатуры текст и выводит отсортированные по алфавиту слова данного текста.


class Text:
    def __init__(self, text):
        self.text = text.split(' ')
        self.text = sorted(self.text)

    # def sorter(self):
    #     for i in sorted(self.text):
    #         print(f"i = {i}")


def main():
    text = Text('c a b d g l m n')
    print(f"Init: {text.text}")
    # text.sorter()
    name = Text('HARRY CHARLIE OSCAR JAMES RUBY HANNAH MAYA JASMINE ARTHUR ZARA DEXTER LYDIA')
    print(f"Names: {name.text}")


if __name__ == '__main__':
    main()
